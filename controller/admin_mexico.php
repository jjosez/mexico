<?php

/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2015-2016  Carlos Garcia Gomez  neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of admin_mexico
 *
 * @author Felipe Flores fflores@balamti.com.mx
 */

require_model('impuesto.php');

/**
 * Description of admin_argentina
 *
 * @author carlos
 */
class admin_mexico extends fs_controller
{
    public $impuestos_ok;

    public function __construct()
    {
        parent::__construct(__CLASS__, 'Mexico', 'admin');
    }

    protected function private_core()
    {
        $this->traducciones();

        if (isset($_GET['opcion'])) {
            if ($_GET['opcion'] == 'moneda') {
                $this->empresa->coddivisa = 'MXN';

                if ($this->empresa->save()) {
                    $this->new_message('Datos guardados correctamente.');
                }
            } elseif ($_GET['opcion'] == 'pais') {
                $this->empresa->codpais = 'MEX';

                if ($this->empresa->save()) {
                    $this->new_message('Datos guardados correctamente.');
                }
            } elseif ($_GET['opcion'] == 'regimenes') {
                $fsvar = new fs_var();

                if ($fsvar->simple_save('cliente::regimenes_iva', 'General,Exento')) {
                    $this->new_message('Datos guardados correctamente.');
                }
            } elseif ($_GET['opcion'] == 'impuestos') {
                $this->set_impuestos();
            }
        } else {
            $this->check_ejercicio();
            $this->share_extensions();
        }
    }

    private function share_extensions()
    {
        $fsext = new fs_extension();
        $fsext->name = 'plan_contable_mexico';
        $fsext->from = __CLASS__;
        $fsext->to = 'contabilidad_ejercicio';
        $fsext->type = 'fuente';
        $fsext->text = 'Plan contable mexico';
        $fsext->params = 'plugins/mexico/extras/mexico.xml';
        $fsext->save();
    }

    private function check_ejercicio()
    {
        $ej0 = new ejercicio();

        foreach ($ej0->all_abiertos() as $ejercicio) {
            if ($ejercicio->longsubcuenta != 6) {
                $ejercicio->longsubcuenta = 6;

                if ($ejercicio->save()) {
                    $this->new_message('Datos del ejercicio ' . $ejercicio->codejercicio . ' modificados correctamente.');
                } else {
                    $this->new_error_msg('Error al modificar el ejercicio.');
                }
            }
        }
    }

    public function regimenes_ok()
    {
        $fsvar = new fs_var();
        $regimenes = $fsvar->simple_get('cliente::regimenes_iva');

        if ($regimenes == 'General,Exento') {
            return true;
        } else {
            return false;
        }
    }

    public function ejercicio_ok()
    {
        $ok = false;

        $ej0 = new ejercicio();
        $ejerccio = $ej0->get_by_fecha($this->today());

        if ($ejerccio) {
            $subc0 = new subcuenta();

            foreach ($subc0->all_from_ejercicio($ejerccio->codejercicio) as $sc) {
                $ok = true;
                break;
            }
        }

        return $ok;
    }

    public function impuestos_ok()
    {
        $ok = false;
        $imp0 = new impuesto();

        foreach ($imp0->all() as $i) {
            if ($i->codimpuesto == '002') {
                $ok = true;
                break;
            }
        }

        return $ok;
    }

    private function set_impuestos()
    {
        /// eliminamos los impuestos que ya existen (los de España)
        $imp0 = new impuesto();

        foreach ($imp0->all() as $impuesto) {
            $this->desvincular_articulos($impuesto->codimpuesto);
            $impuesto->delete();
        }

        /// añadimos los de Mexico
        $codimp = array("001", "002", "003");
        $desc = array("ISR 30%", "IVA 16%", "IEPS 35%");
        $recargo = 0;
        $iva = array(30, 16, 35);
        $cant = count($codimp);

        for ($i = 0; $i < $cant; $i++) {
            $impuesto = new impuesto();
            $impuesto->codimpuesto = $codimp[$i];
            $impuesto->descripcion = $desc[$i];
            $impuesto->recargo = $recargo;
            $impuesto->iva = $iva[$i];
            $impuesto->save();
        }

        $this->impuestos_ok = true;
        $this->new_message('Impuestos de Mexico añadidos.');
    }

    private function desvincular_articulos($codimpuesto)
    {
        $sql = "UPDATE articulos SET codimpuesto = null WHERE codimpuesto = "
        . $this->empresa->var2str($codimpuesto) . ';';

        if ($this->db->table_exists('articulos')) {
            $this->db->exec($sql);
        }
    }

    public function traducciones()
    {
        $clist = array();

        $include = array(
            'albaran', 'albaranes', 'cifnif', 'irpf',
        );

        $tradMX = array(
            'recibo', 'recibos', 'RFC', 'ISR',
        );

        $x = 0;

        foreach ($GLOBALS['config2'] as $i => $value) {
            if (in_array($i, $include)) {
                $clist[] = array('tradMX' => $tradMX[$x], 'nombre' => $i, 'valor' => $value);
                $x++;
            }
        }

        return (array) $clist;
    }
}
